"use strict";
var fetch = require("node-fetch");
var csvToJson = require("csvtojson");
module.exports.parse = async (url, kpiName) => {
  if (url !== "undefined" && url !== "" ) {
    const response = await fetch(url, { method: "GET" });
    const data = await response.text();

    let parsedJson = await csvToJson({
      noheader: false,
      colParser: {
        column1: "omit",
      },
      output: "json",
    }).fromString(data);

    return { [kpiName]: parsedJson };
  }
  return { [kpiName]: [] };
  
};
