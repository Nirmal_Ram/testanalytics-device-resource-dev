"use strict";
module.exports.symbol = (kpiName) => {
  const unit = {
    "CPUUsed (%)": "%",
    "Temp (C)": "C",
    Voltage: "V",
    "Level (%)": "%",
    "Current (A)": "A",
    "Battery drain (mAH)": "mAH",
    "Cumulative battery drain (mAH)": "mAH",
    "ApplicationUsedMemory (KB)": "KB",
    "TotalUsedMemory (KB)": "KB",
    "TotalMemory (KB)": "KB",
    "ApplicationUsedMemory (%)": "%",
    "Janky frame (count)": "/s",
    "Slow UI thread (count)": "/s",
    "FrameDeadLineMissed (count)": "/s",
    "Frame Render Time_90 percentile (ms)": "ms",
    "Frame Render Time_95 percentile (ms)": "ms",
    "Frame Render Time_99 percentile (ms)": "ms",
    "FPS (count)": "/s",
    "fps" : "/s",
    "frames" : "/s",
    "Time(S)" : "s",
    "max time(ms)" : "ms",
    "waiting time(S)" : "s",
    "wait times" : "/s"
  };
  return unit[kpiName];
};
