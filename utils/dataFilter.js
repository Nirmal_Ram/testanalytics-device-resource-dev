"use strict";
module.exports.filter = (arr, kpiName, Xaxis, date) => {
  let offset = date.split("+")[1];
  let result = [];
  arr.map((itm) => {
    result.push({
      TimeStamp: itm[`${Xaxis}`].replace(" ", "T") + "+" + offset,
      [`${kpiName}`]: itm[`${kpiName}`],
    });
  });
  return result;
};
