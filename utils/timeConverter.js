"use strict";
const cityTimezones = require("city-timezones");
module.exports.GMTConverter = (timestamp, city) => {
  let TranslateTime = timestamp.replace(" ", "T") + "Z";
  const timeZoneLookup = cityTimezones.lookupViaCity(city);
  const result = new Date(TranslateTime).toLocaleString(undefined, {
    timeZone: timeZoneLookup[0].timezone,
  });
  return new Date(result).toLocaleString('en-GB');
};
