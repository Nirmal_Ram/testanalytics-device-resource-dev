"use strict";
module.exports.extract = (url) => {
    let filename= url.split(/[#?]/)[0].split('/').pop().trim();
    let timestamp = filename.replace("img_","").split(".")[0]
    return timestamp
};
