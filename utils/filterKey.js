"use strict";
module.exports.filter = (arr, list) => {
  if (arr.length != 0) {
    let keys = Object.keys(arr[0]);
    return list.filter((itm) => {
      return keys.includes(itm);
    });
  }else{
    return []
  }
};
