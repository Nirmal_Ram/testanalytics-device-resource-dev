"use strict";
module.exports.calc = (arr, kpiName, prop) => {
  let result = 0;
  let tempArray = [];
  if (prop === "avg" && arr.length!=0) {
    let count = 0;
    arr.map((itm) => {
      count += Number(itm[`${kpiName}`]);
    });
    result = count / arr.length;
  } else if (prop === "max" && arr.length!=0) {
    arr.map((itm) => {
      tempArray.push(Number(itm[`${kpiName}`]));
    });
    result = Math.max(...tempArray);
  } else if (prop === "min" && arr.length!=0) {
    arr.map((itm) => {
      tempArray.push(Number(itm[`${kpiName}`]));
    });
    result = Math.min(...tempArray);
  }

  return Number(result.toFixed(2));
};
