"use strict";
const dbjs = require("./db.js");
const CSVparser = require("./utils/parser.js");
const DataFilter = require("./utils/dataFilter.js");
const Calculate = require("./utils/calculate.js");
const filterKey = require("./utils/filterKey.js");
const Unit = require("./utils/unit.js");
const timer = require("./utils/timeConverter.js");
const getTimestamp = require("./utils/getTimeFromURL.js");

exports.handler = async (event) => {
  const db = await dbjs.get();
  const id = event.params.path.testUUID;
  const resourceRequested = event.params.path.resourcename;
  const DBresult = await db
    .collection("hotstar-testAnalytics")
    .find({ "uuid._id": id })
    .project({ uuid: 1, info: 1 })
    .toArray();
  // get URL
  let Output;
  let url;
  let ApprovedXaxis;
  let ApprovedYaxis;
  let getURL = Object.entries(DBresult[0].info.testOutputArtifacts).filter(
    (itm) => {
      if (
        itm[1].hasOwnProperty("displayOptions") &&
        itm[1].displayOptions.name === resourceRequested
      ) {
        return itm;
      }
    }
  );
  if (
    resourceRequested != "screenshots" &&
    resourceRequested != "screenrecord"
  ) {
    if (getURL.length != 0) {
      url = getURL[0][1].url;
      // Approved Kpi fields
      ApprovedXaxis = [getURL[0][1].displayOptions.xaxis];
      ApprovedYaxis = getURL[0][1].displayOptions.columns;
    } else {
      url = "";
      ApprovedXaxis = [];
      ApprovedYaxis = [];
    }

    // CSV parsing
    var Kpi = await CSVparser.parse(url, "columns");

    //filtered Kpi list
    let KpiList;
    let KpiXaxis;
    if (Kpi.length != 0) {
      KpiList = filterKey.filter(Kpi.columns, ApprovedYaxis);
      KpiXaxis = filterKey.filter(Kpi.columns, ApprovedXaxis);
    } else {
      KpiList = [];
      KPiXaxis = [];
    }
    //timezone convertion
    // if (DBresult[0].testInformation.hasOwnProperty("city")) {
    //   var city = DBresult[0].testInformation.city;
    // } else {
    //   var city = "mumbai";
    // }

    const exist = await db
      .collection("hotstar-testAnalytics")
      .find({ "uuid._id": id, "info.performanceKpi": { $exists: true } })
      .toArray();

    if (exist.length == 0) {
      const post = await db
        .collection("hotstar-testAnalytics")
        .updateOne({ "uuid._id": id }, { $set: { "info.performanceKpi": {} } });
    }

    const exists = await db
      .collection("hotstar-testAnalytics")
      .findOne({ "uuid._id": id });

    if (!exists.info.performanceKpi.hasOwnProperty(resourceRequested)) {
      let Response = KpiList.map((kpi) => {
        return {
          name: kpi,
          unit: Unit.symbol(kpi),
          value: {
            avg: Calculate.calc(Kpi.columns, kpi, "avg"),
            max: Calculate.calc(Kpi.columns, kpi, "max"),
            min: Calculate.calc(Kpi.columns, kpi, "min"),
          },
        };
      });
      let key = "info.performanceKpi." + resourceRequested;
      const post = await db
        .collection("hotstar-testAnalytics")
        .updateOne({ "uuid._id": id }, { $set: { [key]: Response } });
    }
    const DBresponse = await db
      .collection("hotstar-testAnalytics")
      .findOne({ "uuid._id": id });
    Output = {
      version: "1.2",
      uuid: {
        _id: DBresponse.uuid._id,
        testId: DBresponse.uuid.testId,
        sessionId: DBresponse.uuid.sessionId,
        userDisplayName: DBresponse.uuid.userDisplayName,
        userEmail: DBresponse.uuid.userEmail,
        username: DBresponse.uuid.username,
      },
      columns: KpiList.map((kpi) => {
        return {
          name: kpi,
          unit: Unit.symbol(kpi),
          value: {
            avg: Calculate.calc(Kpi.columns, kpi, "avg"),
            max: Calculate.calc(Kpi.columns, kpi, "max"),
            min: Calculate.calc(Kpi.columns, kpi, "min"),
          },
          datas: DataFilter.filter(Kpi.columns, kpi, KpiXaxis[0] , DBresponse.info.testStartTime),
        };
      }),
    };
  } else if (resourceRequested === "screenshots") {
    const DBresponse = await db
      .collection("hotstar-testAnalytics")
      .findOne({ "uuid._id": id });
    Output = {
      version: "1.2",
      uuid: {
        _id: DBresponse.uuid._id,
        testId: DBresponse.uuid.testId,
        sessionId: DBresponse.uuid.sessionId,
        userDisplayName: DBresponse.uuid.userDisplayName,
        userEmail: DBresponse.uuid.userEmail,
        username: DBresponse.uuid.username,
      },
      columns: [
        {
          name: "Screenshots",
          datas: DBresponse.info.testOutputArtifacts.deviceScreenshots.url.map(
            (imageURL) => {
              return {
                TimeStamp: getTimestamp.extract(imageURL),
                source: "link",
                url: imageURL,
              };
            }
          ),
        },
      ],
    };
  } else if (resourceRequested === "screenrecord") {
    const DBresponse = await db
      .collection("hotstar-testAnalytics")
      .findOne({ "uuid._id": id });
    Output = {
      version: "1.2",
      uuid: {
        _id: DBresponse.uuid._id,
        testId: DBresponse.uuid.testId,
        sessionId: DBresponse.uuid.sessionId,
        userDisplayName: DBresponse.uuid.userDisplayName,
        userEmail: DBresponse.uuid.userEmail,
        username: DBresponse.uuid.username,
      },
      columns: [
        {
          name: "Screenrecord",
          datas: DBresponse.info.testOutputArtifacts.deviceScreenRecord.url.map(
            (VideoURL) => {
              return {
                source: "link",
                url: VideoURL,
              };
            }
          ),
        },
      ],
    };
  }
  const response = {
    statusCode: 200,
    body: Output,
    headers: {
      "Access-Control-Allow-Headers":
        "Content-Type, Origin, X-Requested-With, Accept, Authorization, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Access-Control-Allow-Origin",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET,PUT",
    },
  };
  return response;
};
